//
//  ViewController.swift
//  KasaLib
//
//  Created by Suleyman Calik on 13.11.2015.
//  Copyright © 2015 Kasa. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let now = Date()
        let date2 = (Calendar.current as NSCalendar).date(byAdding: NSCalendar.Unit.day, value:-20, to:now, options:NSCalendar.Options())!
        
        let str1 = now.dayString()
        let str2 = date2.dayString()
        
        print(str1)
        print(str2)
        
        
        let str3 = now.weekString()
        let str4 = date2.weekString()
        
        print(str3)
        print(str4)
        
        
        let str5 = now.monthString()
        let str6 = date2.monthString()
        print(str5)
        print(str6)
        

    }




}

