# Kasa

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KasaTest is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "KasaTest"
```

## Author

Suleyman Calik, suleymancalik@gmail.com

## License

KasaTest is available under the MIT license. See the LICENSE file for more info.
