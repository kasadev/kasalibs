//
//  LoginDTO.swift
//  Kasa
//
//  Created by Suleyman Calik on 22/05/15.
//  Copyright (c) 2015 Kasa. All rights reserved.
//

import ObjectMapper

public class LoginDTO: BaseDTO {
   
    public var user:User!
    public required init?(map: Map) {
        super.init(map: map)
    }
//    public required convenience init?(_ map: Map){
//        self.init()
//    }

    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        user    <-  map["data"]
    }
}
