//
//  CompaniesDTO.swift
//  Kasa
//
//  Created by Suleyman Calik on 03/07/15.
//  Copyright (c) 2015 Calik. All rights reserved.
//

import ObjectMapper

public class CompaniesDTO: BaseDTO {
   
    public var data:[Company]!
    
//    public required convenience init?(_ map: Map){
//        self.init()
//    }
    public required init?(map: Map) {
        super.init(map: map)
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        data    <-  map["data"]
    }

}
