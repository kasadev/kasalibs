//
//  UserDTO.swift
//  Kasa
//
//  Created by Suleyman Calik on 25/05/15.
//  Copyright (c) 2015 Kasa. All rights reserved.
//

import ObjectMapper

public class UserDTO: BaseDTO {
   
    public var data:User!
    
    public required init?(map: Map) {
        super.init(map: map)
    }

    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        data    <-  map["data"]
    }
}
