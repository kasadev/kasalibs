//
//  ShopDTO.swift
//  KasaLib
//
//  Created by Suleyman Calik on 9.01.2016.
//  Copyright © 2016 Kasa. All rights reserved.
//

import ObjectMapper

public class ShopDTO: BaseDTO {

    public var data:Shop!
    
//    public required convenience init?(_ map: Map){
//        self.init()
//    }
    public required init?(map: Map) {
        super.init(map: map)
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        data    <-  map["data"]
    }
    
}
