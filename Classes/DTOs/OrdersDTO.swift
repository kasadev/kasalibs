//
//  OrdersDTO.swift
//  Kasa
//
//  Created by Suleyman Calik on 21/05/15.
//  Copyright (c) 2015 Kasa. All rights reserved.
//

import ObjectMapper

public class OrdersDTO:BaseDTO {
    
    public var data:[Order]!
    
//    public required convenience init?(_ map: Map){
//        self.init()
//    }
    public required init?(map: Map) {
        super.init(map: map)
    }

    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        data    <-  map["data"]
    }
    
}
