//
//  CategoriesDTO.swift
//  Kasa
//
//  Created by Suleyman Calik on 22/05/15.
//  Copyright (c) 2015 Kasa. All rights reserved.
//

import ObjectMapper

public class CategoriesDTO: BaseDTO {
   
    public var data:[ProductCategory]!
    
    public required init?(map: Map) {
        super.init(map: map)
    }
//    public required convenience init?(_ map: Map){
//        self.init()
//    }

    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        data    <-  map["data"]
    }
}
