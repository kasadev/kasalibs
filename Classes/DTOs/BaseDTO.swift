//
//  BaseDTO.swift
//  Kasa
//
//  Created by Suleyman Calik on 21/05/15.
//  Copyright (c) 2015 Kasa. All rights reserved.
//

import ObjectMapper

public class BaseDTO:Mappable {
    
    public var result:String!
    public var httpCode:Int!
    public var error:MKError!
    
    required public init?(map: Map) {
        
    }
    
//    required public init?(map: Map) {
//        self.init()
//    }
//    
    public func mapping(map: Map) {
        result      <-  map["result"]
        httpCode    <-  map["http_code"]
        error       <-  map["error"]
    }
}
