//
//  ColorsDTO.swift
//  Kasa
//
//  Created by Suleyman Calik on 22/05/15.
//  Copyright (c) 2015 Kasa. All rights reserved.
//

import ObjectMapper

public class ColorsDTO: BaseDTO {

    public var data:[Color]!
    
//    public required convenience init?(_ map: Map){
//        self.init()
//    }
    public required init?(map: Map) {
        super.init(map: map)
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        data    <-  map["data"]
    }
}
