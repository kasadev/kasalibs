//
//  MKService.swift
//  KasaLib
//
//  Created by Suleyman Calik on 25.01.2016.
//  Copyright © 2016 Kasa. All rights reserved.
//

import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


public class MKService {
    
    public class func createURL(_ query:MKQuery, onlyActive:Bool = true) -> String {
        let entityType = query.type
        var url = entityType.url()
        
        if query.whereQueries == nil {
            query.whereQueries = []
        }
        
        if onlyActive {
            query.whereQueries.append(WhereQuery(key:"active", value:"true" as AnyObject, statement:.equals))
        }
        
        var filterStr = ""
        
        if query.whereQueries?.count > 0 {
            filterStr += "\"where\":{\"and\":["
            
            for (index,query) in query.whereQueries.enumerated() {
                if index > 0 {
                    filterStr += ","
                }
                
                filterStr += query.description
            }
            
            filterStr += "]}"
            
        }
        
        if query.orderRules?.count > 0 {
            
            if filterStr.characters.count > 0 {
                filterStr += ","
            }
            
            filterStr += "\"order\":["
            
            for (index,rule) in query.orderRules.enumerated() {
                if index > 0 {
                    filterStr += ","
                }
                
                let order = rule.ascending ? "asc" : "desc"
                filterStr += "\"\(rule.orderBy) \(order)\""
            }
            
            filterStr += "]"
        }
        
        if let skip = query.skip {
            if filterStr.characters.count > 0 {
                filterStr += ","
            }
            
            filterStr += "\"skip\":\(skip)"
        }
        
        if let limit = query.limit {
            if filterStr.characters.count > 0 {
                filterStr += ","
            }
            filterStr += "\"limit\":\(limit)"
        }
        
        
        if filterStr.characters.count > 0 {
            let filterFinal = "filter={" + filterStr + "}"
            print(filterFinal)
            url += "?" + filterFinal
        }
        
        
        return url
    }
    
}
