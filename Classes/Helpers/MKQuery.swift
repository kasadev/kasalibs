//
//  MKQuery.swift
//  Kasa
//
//  Created by Suleyman Calik on 19/03/15.
//  Copyright (c) 2015 Kasa. All rights reserved.
//

import UIKit
public class MKQuery: NSObject {
   
    var type:MKObject.Type
    var whereQueries:[WhereQuery]!
    var orderRules:[MKOrderRule]!
    var skip:Int!
    var limit:Int!
    
    public init(type:MKObject.Type) {
        self.type = type
    }

    public init(type:MKObject.Type, whereQueries:[WhereQuery]? = nil, orderRules:[MKOrderRule]? = nil, skip:Int? = nil, limit:Int? = nil) {
        self.type = type
        self.whereQueries = whereQueries
        self.orderRules = orderRules
        self.limit = limit
        self.skip = skip
    }
    
    
    public class func shopQuery(_ shop:Shop, type:MKObject.Type) -> MKQuery {
        let whereQuery = WhereQuery(key:"shopId", value:shop.id as AnyObject, statement:WhereQuery.Where.equals)
        let query = MKQuery(type:type, whereQueries:[whereQuery], orderRules:nil, skip:nil, limit:nil)
        return query
    }

    
    public class func shopQuery(_ type:MKObject.Type) -> MKQuery {
        var whereQueries:[WhereQuery]!
        if let shop = Shop.currentShop() {
            whereQueries = [WhereQuery(key:"shopId", value:shop.id as AnyObject, statement:WhereQuery.Where.equals)]
        }
        else {
            print("NO CURRENT SHOP!")
        }
        
        let query = MKQuery(type:type, whereQueries:whereQueries, orderRules:nil, skip:nil, limit:nil)
        return query
    }
    
    public func onlyActive() -> MKQuery {
        let whereQ = WhereQuery(key:"active", value:true as AnyObject, statement:WhereQuery.Where.equals)
        if let _ = whereQueries {
            whereQueries.append(whereQ)
        }
        else {
            whereQueries = [whereQ]
        }

        return self
    }
    
    public func onlyNewer() -> MKQuery {
        if let newestObject = type.objects(type, sorting:(sortBy:"updatedAt", ascending:false)).first {
            let whereQ = WhereQuery(key:"updatedAt", value:newestObject.updatedAt as AnyObject, statement:WhereQuery.Where.greaterThan)
            if let _ = whereQueries {
                whereQueries.append(whereQ)
            }
            else {
                whereQueries = [whereQ]
            }
        }
        
        return self
    }
}


public class WhereQuery: NSObject {

    public enum Where:CustomStringConvertible {
        case equals
        case greaterThan
        case lessThan
        case greaterThanOrEquals
        case lessThanOrEquals
        case between
        case inq
        
        public var description : String {
            get {
                switch(self) {
                case .equals:               return "="
                case .greaterThan:          return "gt"
                case .lessThan:             return "lt"
                case .greaterThanOrEquals:  return "gte"
                case .lessThanOrEquals:     return "lte"
                case .between:              return "between"
                case .inq:                  return "inq"
                }
            }
        }

    }
    
    var key:String
    var value:AnyObject
    var statement:Where
    
    public init(key:String, value:Any, statement:Where) {
        self.key = key
        self.value = value as AnyObject
        self.statement = statement
    }
    

    public override var description: String {
        
        var valueObj:AnyObject!
        if value is String {
            valueObj = "\(value)" as AnyObject!
        }
        else if value is Date {
            valueObj = "\((value as! Date).serverString())" as AnyObject!
        }
        else if statement == .between || statement == .inq {
            if let values = value as? [AnyObject] {
                var valueStr = ""
                for (index,aValue) in values.enumerated() {
                    if index > 0 {
                        valueStr.append(",")
                    }
                    if let str = aValue as? String {
                        valueStr.append(str)
                    }
                    else if let date = aValue as? Date {
                        valueStr.append(date.serverString())
                    }
                }
                let arr = [valueStr]
                valueObj = arr as AnyObject!
            }
            else {
                return ""
            }
        }
        
        let dictVal:Any = statement == .equals ? valueObj : [statement.description : valueObj]
        let dict = [key:dictVal]
        
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options:[])
            
            if let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as? String {
                return jsonString
            }
            else {
                return ""
            }
        } catch {
            print("json error: \(error)")
            return ""
        }
    }
    
    
}

