//
//  MKExtensions.swift
//  Kasa
//
//  Created by Suleyman Calik on 6.11.2014.
//  Copyright (c) 2014 Kasa. All rights reserved.
//

import UIKit

public extension String {

    public func isValidEmail() -> Bool {

        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: self)
        return result
    }

    public subscript (i: Int) -> Character {
        return self[self.characters.index(self.startIndex, offsetBy: i)]
    }
    
    public subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    public subscript (r: Range<Int>) -> String {
        return substring(with: (characters.index(startIndex, offsetBy: r.lowerBound) ..< characters.index(startIndex, offsetBy: r.upperBound)))
    }
    
    /**
    - returns: Base64 encoded string
    */
    public func encodeToBase64Encoding() -> String {
        let utf8str = self.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        return utf8str.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
    }
}

public extension NSObject {
    
    public class func className() -> String {
        let components:Array<String> = self.description().components(separatedBy: ".")
        if components.count > 0 {
            return components.last!
        }
        else {
            return ""
        }
    }
}


public func dispatch(_ handler:@escaping ()->(), afterDelay delay:Double) {
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC),execute: handler)
}


public extension UIColor {
    
    public convenience init(R: Int, G: Int, B: Int) {
        self.init(red:CGFloat(R)/255.0, green:CGFloat(G)/255.0, blue:CGFloat(B)/255.0, alpha:1.0)
    }

    
    public class func randomColor() -> UIColor {
        
        let randomRed:CGFloat = CGFloat(drand48())
        
        let randomGreen:CGFloat = CGFloat(drand48())
        
        let randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
    }

}

public extension UIImage {
    
    public class func imageWithColor(_ color:UIColor) -> UIImage {
            let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
            UIGraphicsBeginImageContext(rect.size)
            let context = UIGraphicsGetCurrentContext()
            
            context?.setFillColor(color.cgColor)
            context?.fill(rect)
            
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return image!
    }
}

public extension Dictionary {
    /**
    Checks if a key exists in the dictionary.
    
    - parameter key: Key to check
    - returns: true if the key exists
    */
    public func has (_ key: Key) -> Bool {
        return index(forKey: key) != nil
    }
    
    public func sortedKeys(_ isOrderedBefore:(Key,Key) -> Bool) -> [Key] {
        var array = Array(self.keys)
        array.sort(by: isOrderedBefore)
        return array
    }
    
    // Slower because of a lot of lookups, but probably takes less memory (this is equivalent to Pascals answer in an generic extension)
    public func sortedKeysByValue(_ isOrderedBefore:(Value, Value) -> Bool) -> [Key] {
        return sortedKeys {
            isOrderedBefore(self[$0]!, self[$1]!)
        }
    }
    
    // Faster because of no lookups, may take more memory because of duplicating contents
    public func keysSortedByValue(_ isOrderedBefore:(Value, Value) -> Bool) -> [Key] {
        var array = Array(self)
        array.sort {
            let (_, lv) = $0
            let (_, rv) = $1
            return isOrderedBefore(lv, rv)
        }
        return array.map {
            let (k, _) = $0
            return k
        }
    }
    
    /// Merges the dictionary with dictionaries passed. The latter dictionaries will override
    /// values of the keys that are already set
    ///
    /// :param dictionaries A comma seperated list of dictionaries
    public mutating func merge<K, V>(_ dictionaries: Dictionary<K, V>...) {
        for dict in dictionaries {
            for (key, value) in dict {
                self.updateValue(value as! Value, forKey: key as! Key)
            }
        }
    }    
    
}

public extension Array {
    /**
    Creates a dictionary composed of keys generated from the results of
    running each element of self through groupingFunction. The corresponding
    value of each key is an array of the elements responsible for generating the key.
    
    - parameter groupingFunction:
    - returns: Grouped dictionary
    */
    public func groupBy <U> (groupingFunction group: (Element) -> U) -> [U: Array] {
        
        var result = [U: Array]()
        
        for item in self {
            
            let groupKey = group(item)
            
            // If element has already been added to dictionary, append to it. If not, create one.
            if result.has(groupKey) {
                result[groupKey]! += [item]
            } else {
                result[groupKey] = [item]
            }
        }
        
        return result
    }
    
    /**
    Similar to groupBy, instead of returning a list of values,
    returns the number of values for each group.
    
    - parameter groupingFunction:
    - returns: Grouped dictionary
    */
    public func countBy <U> (groupingFunction group: (Element) -> U) -> [U: Int] {
        
        var result = [U: Int]()
        
        for item in self {
            let groupKey = group(item)
            
            if result.has(groupKey) {
                result[groupKey]! += 1
            } else {
                result[groupKey] = 1
            }
        }
        
        return result
    }
}

public extension Double {
    
    public func format(_ f: String) -> String
    {
        return String(format: "%\(f)f", self)
    }
    
    public mutating func roundTo(_ f: String)
    {
        self = NSString(format: "%\(f)f" as NSString, self).doubleValue
    }
    
    static let formatter = FormatterBuilder.buildDefaultCurrencyNumberFormatter()

    public func TLRepresentation(_ _digits:Int! = 2) -> String {
        
        if let formattedPrice = Double.formatter.string(from: NSNumber(value:self)) {
            return formattedPrice
        }
        return "-"
    }

    /*
    public func TLRepresentation(_ _digits:Int! = 2) -> String {
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale.current
        
        formatter.maximumFractionDigits = _digits
        
        var formattedPrice = formatter.string(from: NSNumber())!
        formattedPrice = formattedPrice.replacingOccurrences(of: "TRY", with: "₺")
        formattedPrice = formattedPrice.replacingOccurrences(of: "$", with: "₺")
        return formattedPrice
        
    }
    */
    
    public func percentageRepresentation() -> String {
        
        if floor(self) == self {
            //Tam sayı
            return "%\(Int(self))"
        }
        return "%\(self)"
    }
}

/*
extension UITextField {
    
    func invariantDoubleValue() -> Double {
        return NSString(string:NSString(string:self.text!).stringByReplacingOccurrencesOfString(",", withString: ".")).doubleValue
    }

    func checkNumericInput(shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if self.keyboardType == UIKeyboardType.NumberPad || self.keyboardType == UIKeyboardType.DecimalPad {
            let nonNumericInput = NSCharacterSet(charactersInString: "0123456789.-").invertedSet
            if let _ = string.rangeOfCharacterFromSet(nonNumericInput) {
                return false
            }
        }
        return true
    }
}
*/

public extension Date {
    
    public func formattedString(_ format:String, timeZone:TimeZone! = TimeZone(identifier:"UTC")) -> String {
        let dateFormat = DateFormatter()
        dateFormat.timeZone = timeZone
        dateFormat.dateFormat = format
        return dateFormat.string(from: self)
    }
    
    public func displayString (_ timeZone:TimeZone! = TimeZone(identifier:"UTC")) -> String {
        return formattedString("dd.MM.yyyy HH:mm", timeZone:timeZone)
    }
    
    public func shortDateString(_ timeZone:TimeZone! = TimeZone(identifier:"UTC")) -> String {
        return formattedString("dd.MM.yyyy", timeZone:timeZone)
    }
    
    
    func timeString(_ timeZone:TimeZone! = TimeZone(identifier:"UTC")) -> String {
        return formattedString("HH:mm", timeZone:timeZone)
    }

    
    public func dayString (_ includeDayText:Bool = true) -> String {
        let day = getDayOfMonth()
        let month = getMonth()
        let weekday = getDayOfWeek()
        
        let year = getYear()
        let thisYear = Date().getYear()
        let yearStr = year == thisYear ? "" : "\( year)"
        
        let dayText = includeDayText ? " \(textofWeekday(weekday))" : ""
        let str = "\(day) \(textOfMonth(month))\(yearStr)\(dayText)"
        return str
    }
    
    public func weekString () -> String {
        let week = Date.rangeOfWeek(self)
        
        let begin = week.0
        let beginDay = begin.getDayOfMonth()
        let beginMonth = begin.textOfMonth(begin.getMonth())
        
        let end = week.1
        let endDay = end.getDayOfMonth()
        let endMonth = end.textOfMonth(end.getMonth())
        
        return "\(beginDay) \(beginMonth) - \(endDay) \(endMonth)"
    }
    
    public func monthString() -> String {
        return textOfMonth(getMonth())
    }
    
    public func serverString(_ timeZone:TimeZone! = TimeZone(identifier:"UTC")) -> String {
        return formattedString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'", timeZone:timeZone) as String
    }
    
    
    
    public static func dateFromServerString(_ dateStr:String, timeZone:TimeZone! = TimeZone(identifier:"UTC")) -> Date? {
        var date = Date.dateFromDisplayString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'", dateString:dateStr, timeZone:timeZone)
        if date == nil {
            date = Date.dateFromDisplayString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'", dateString:dateStr, timeZone:timeZone)
        }
        return date
    }

    //
    
    public static func dateFromDisplayString (_ format: String, dateString:String?, timeZone:TimeZone! = TimeZone(identifier:"UTC")) -> Date? { //! = NSTimeZone(name:"UTC")
        if dateString == nil {
            return nil
        }
        
        let dateFormat = DateFormatter()
        dateFormat.timeZone = timeZone
        dateFormat.dateFormat = format
//        dateFormat.locale = NSLocale.systemLocale()
        //dateFormat.formatterBehavior = NSDateFormatterBehavior.BehaviorDefault
        let date = dateFormat.date(from: dateString!)
        return date
    }
    
    
    public static func dateFromDisplayString (_ displayString:String?) -> Date? {
        if displayString == nil {
            return nil
        }
        
        let dateFormat = DateFormatter()
        dateFormat.timeZone = TimeZone.current
        dateFormat.locale = Locale.current
        dateFormat.dateFormat = "dd.MM.yyyy hh:mm"
        //dateFormat.formatterBehavior = NSDateFormatterBehavior.BehaviorDefault
        let date = dateFormat.date(from: displayString!)
        return date
    }

    
    func getUnit(_ unit:NSCalendar.Unit) -> Int {
        
        let cal = Calendar.current
        var constant = 0

        let units:NSCalendar.Unit =
        [.year, .month, .weekOfYear, .weekday, .day, .hour, .minute, .second]
        
        let comp = (cal as NSCalendar).components(units, from:self)
        
        switch unit {
        case NSCalendar.Unit.second:
            constant =  comp.second!
        case NSCalendar.Unit.minute:
            constant =  comp.minute!
        case NSCalendar.Unit.hour:
            constant =  comp.hour!
        case NSCalendar.Unit.day:
            constant =  comp.day!
        case NSCalendar.Unit.weekday:
            constant =  comp.weekday!
        case NSCalendar.Unit.weekOfYear:
            constant =  comp.weekOfYear!
        case NSCalendar.Unit.month:
            constant =  comp.month!
        case NSCalendar.Unit.year:
            constant =  comp.year!
        default: break
        }

        return constant
    }
    
    public func getHour() -> Int {
        return getUnit(NSCalendar.Unit.hour)
    }
    
    public func getMonth() -> Int {
        return getUnit(NSCalendar.Unit.month)
    }
    
    public func getYear() -> Int {
        return getUnit(NSCalendar.Unit.year)
    }

    public func getDayOfMonth() -> Int {
        return getUnit(NSCalendar.Unit.day)
    }
    
    public func getDayOfWeek() -> Int {
        return getUnit(NSCalendar.Unit.weekday)
    }
    
    func textofWeekday(_ dayOfWeek:Int) -> String {
//        let days = ["", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi", "Pazar"]
        let days = Calendar.current.weekdaySymbols
        if dayOfWeek <= days.count {
            if dayOfWeek == 0 {
                return days.last!
            }
            else {
                return days[dayOfWeek - 1]
            }
        }
        else {
            return ""
        }
    }
    
    func textOfMonth(_ month:Int) -> String {
//        let months = ["", "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz" , "Ağustos" , "Eylül" , "Ekim" , "Kasım" , "Aralık"]
        let months = Calendar.current.monthSymbols
        if month <= months.count && month > 0 {
            return months[month - 1]
        }
        else {
            return ""
        }
    }
    
    public func getDayText() -> String {
        let dayOfWeek = getDayOfWeek()
        return textofWeekday(dayOfWeek)
    }
    
    
    func addUnit(_ constant:Int, unit:NSCalendar.Unit) -> Date {
        
        let calendar = Calendar.current
        let units:NSCalendar.Unit =
        [.year, .month, .day, .hour, .minute, .second]
        
        var comp = (calendar as NSCalendar).components(units, from:self)
        switch unit {
        case NSCalendar.Unit.year:
            comp.year = comp.year! + constant
        case NSCalendar.Unit.month:
            comp.month = comp.month! + constant
        case NSCalendar.Unit.day:
            comp.day = comp.day! + constant
        case NSCalendar.Unit.hour:
            comp.hour = comp.hour! + constant
        case NSCalendar.Unit.minute:
            comp.minute = comp.minute! + constant
        case NSCalendar.Unit.second:
            comp.second = comp.second! + constant
        default: break
        }
        return calendar.date(from: comp)!
    }
    
    public func addSeconds(_ seconds:Int) -> Date {
        return addUnit(seconds, unit:NSCalendar.Unit.second)
    }

    public func addMinutes(_ minutes:Int) -> Date {
        return addUnit(minutes, unit:NSCalendar.Unit.minute)
    }

    public func addHours(_ hours:Int) -> Date {
        return addUnit(hours, unit:NSCalendar.Unit.hour)
    }
    
    public func addDays(_ days:Int) -> Date {
        return addUnit(days, unit:NSCalendar.Unit.day)
    }
    
    public func addMonths(_ months:Int) -> Date {
        return addUnit(months, unit:NSCalendar.Unit.month)
    }
    
    public func beginningOfDay() -> Date {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let units:NSCalendar.Unit =
            [.year, .month, .day, .hour, .minute, .second]

        if let comp = (calendar as NSCalendar?)?.components(units, from: self) {
            var compon = comp
            compon.hour = 0
            compon.minute = 0
            compon.second = 0
            return calendar.date(from: compon)!
        }
        return self
    }
    
    
    public func beginningOfWeek() -> Date {
        let calendar = Calendar.current
        let weekdayComponents = (calendar as NSCalendar).components(NSCalendar.Unit.weekday, from:self)
        
        
        var componentsToSubtract = DateComponents()
        componentsToSubtract.day = -(weekdayComponents.weekday! - calendar.firstWeekday)
        
        let beginningOfWeek = (calendar as NSCalendar).date(byAdding: componentsToSubtract, to:self, options:NSCalendar.Options())!
        
        let units:NSCalendar.Unit = [.year, .month, .day]
        
        var components = (calendar as NSCalendar).components(units, from:beginningOfWeek)
        
        components.hour = 0
        components.minute = 0
        components.second = 0
        
        return calendar.date(from: components)!
    }
    
    public func beginningOfMonth() -> Date {
        let day = getDayOfMonth()
        return addDays(-(day-1)).beginningOfDay()
    }
    
    public func endOfWeek() -> Date {
        let calendar = Calendar.current
        let units:NSCalendar.Unit = [.year, .month, .day]
        var dateComponents = (calendar as NSCalendar).components(units, from:self)
        dateComponents.weekday = 7
        dateComponents.weekdayOrdinal = 7
        return calendar.date(from: dateComponents)!
    }
    
    public func endOfDay() -> Date {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let units:NSCalendar.Unit =
        [.year, .month, .day, .hour, .minute, .second]
        
        if let comp = (calendar as NSCalendar?)?.components(units, from: self) {
            var compon = comp
            compon.hour = 23
            compon.minute = 59
            compon.second = 59
            return calendar.date(from: compon)!
        }
        return self
    }
    
    public static func rangeOfDay(_ date:Date) -> (Date,Date) {
        let dayBegin = date.beginningOfDay()
        let dayEnd = date.endOfDay()
        return (dayBegin, dayEnd)
    }
    
    public static func rangeOfWeek(_ date:Date) -> (Date,Date) {
        let begin = date.beginningOfWeek()
        let end = begin.addDays(7).addSeconds(-1)
        return (begin,end)
    }
    
    public static func rangeOfThisWeek() -> (Date,Date) {
        return rangeOfWeek(Date())
    }

    public static func rangeOfLastWeek() -> (Date,Date) {
        return rangeOfWeek(Date().addDays(-7))
    }

    public static func rangeOfMonth(_ date:Date) -> (Date,Date) {
        let begin = date.beginningOfMonth()
        let calendar = Calendar.current
        let daysRange = (calendar as NSCalendar).range(of: NSCalendar.Unit.day, in:NSCalendar.Unit.month, for:begin)
        let end = begin.addDays(daysRange.length).addSeconds(-1)
        return (begin,end)
    }
    
    public static func rangeOfThisMonth() -> (Date,Date) {
        return rangeOfMonth(Date())
    }

    public static func rangeOfLastMonth() -> (Date,Date) {
        let aLastMonthDay =  Date().beginningOfMonth().addDays(-1)
        return rangeOfMonth(aLastMonthDay)
    }
    
    public static func rangeOfYesterday() -> (Date,Date) {
        return rangeOfDay(Date().addDays(-1))
    }
    
    public static func rangeOfToday() -> (Date,Date) {
        return rangeOfDay(Date())
    }
    
    
    public static func today() -> Date {
        return Date().beginningOfDay()
    }
    
    public static func yesterday() -> Date {
        return today().addDays(-1)
    }
    
    
    
}
