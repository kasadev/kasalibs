//
//  ReportData.swift
//  Kasa
//
//  Created by Emre Sebat on 05/02/15.
//  Copyright (c) 2015 Kasa. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


public class ReportData: NSObject {
   
    public enum SalesSummaryType: Int, CustomStringConvertible {
        case sales
        case discounts
        case refunds
        // Expenses
        
        public static let allValues = [sales,discounts,refunds]
        
        public var description : String {
            get {
                switch(self) {
                case .sales: return "Satış"
                case .discounts: return "İndirimler"
                case .refunds: return "İadeler"
                }
            }
        }
    }
    
    public var shopPaymentTypes = ShopAmountType.paymentTypes()

    public var dateRange:(Date,Date)?
    
    public var ordersAll: [Order]!
    public var ordersActive: [Order]!
    public var ordersCancelled: [Order]!

    public var orderItems: [OrderItem]!
    public var orderCustomers:[(Int,String,UIColor)]!
    
    public var ordersByHour: [Int:[Order]]!
    public var sortedHours: [Int]!
    
    public var salesAmounts:[SalesSummaryType: Double?]?
    public var paymentAmounts:[AmountType: Double?]?
    public var productAmounts:[String: Double?]?
    public var orderedProductAmountKeys:[String]?
    
    public var salesNumbers:[SalesSummaryType: Int]?
    public var paymentNumbers:[AmountType: Int]?
    public var productNumbers:[String: Int]?
    public var orderedProductNumberKeys:[String]?

    public func loadOrders(_ orderArray:[Order]) {
        
        ordersAll = orderArray
        ordersActive = ordersAll.filter{ $0.cancelled == false }
        ordersCancelled = ordersAll.filter{ $0.cancelled == true }
        
        ordersByHour = ordersActive.groupBy { $0.createdAt.getHour() }
        sortedHours = Array(ordersByHour.keys).sorted(by: <)
        
        var activeItems = [OrderItem]()
        var activeCustomerMen = 0
        var activeCustomerWomen = 0
        var activeCustomerKids = 0
        for order in ordersActive {
            activeItems.append(contentsOf: order.items)
            for customers in order.customers {
                switch customers.customerType {
                case CustomerType.man.rawValue:
                    activeCustomerMen += customers.count
                case CustomerType.woman.rawValue:
                    activeCustomerWomen += customers.count
                case CustomerType.kid.rawValue:
                    activeCustomerKids += customers.count
                default: break
                }
            }
        }
        orderItems = activeItems
        orderCustomers = [
            (activeCustomerMen,"Erkek", UIColor(R:18, G:148, B:248)),
            (activeCustomerWomen,"Kadın", UIColor(R:242, G:10, B:201)),
            (activeCustomerKids,"Çocuk", UIColor(R:13, G:224, B:142))]
        
        shopPaymentTypes = ShopAmountType.paymentTypes()
        calculateAmountsAndNumbers()
    }
    
    
    
    public func calculateAmountsAndNumbers() {
        
        salesAmounts = [SalesSummaryType: Double?]()
        salesAmounts?[SalesSummaryType.sales] = ordersActive.map{ $0.totalAmount }.reduce(0,+)
        salesAmounts?[SalesSummaryType.refunds] = ordersCancelled.map{ $0.totalAmount }.reduce(0,+)
        salesAmounts?[SalesSummaryType.discounts] = orderItems.map{ $0.rawAmount - $0.amount }.reduce(0,+)
        
        salesNumbers = [SalesSummaryType:Int]()
        salesNumbers?[SalesSummaryType.sales] = ordersActive.count
        salesNumbers?[SalesSummaryType.discounts] = orderItems.filter{ $0.rawAmount - $0.amount > 0 }.count
        salesNumbers?[SalesSummaryType.refunds] = ordersCancelled.count
        
        paymentAmounts = [AmountType:Double?]()
        paymentNumbers = [AmountType:Int]()
        for paymentType in shopPaymentTypes {
            paymentAmounts?[paymentType] = ordersActive.filter{ $0.cancelled == false && $0.amountTypeId == paymentType.id }.map{ $0.totalAmount }.reduce(0,+)
            paymentNumbers?[paymentType] = ordersActive.filter{ $0.cancelled == false && $0.amountTypeId == paymentType.id }.count
        }
        
        productAmounts = [String:Double?]()
        productNumbers = [String:Int]()
        var productGroup = orderItems.groupBy{ $0.productId }
        for product in Array(productGroup.keys) {
            productAmounts?[product] = productGroup[product]?.map{ $0.amount }.reduce(0,+)
            productNumbers?[product] = productGroup[product]?.map{ $0.count }.reduce(0,+)
        }
        orderedProductAmountKeys = productAmounts?.keysSortedByValue(>)
        orderedProductNumberKeys = productNumbers?.keysSortedByValue(>)
    }
    

}
