//
//  FormatterBuilder.swift
//  PayPad
//
//  Created by Ismail on 27/08/2016.
//  Copyright © 2016 Kasa. All rights reserved.
//

import UIKit

class FormatterBuilder {
    
    class func buildDefaultCurrencyNumberFormatter() -> NumberFormatter {
        let numberFormatter: NumberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Locale(identifier: "tr_TR")
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.minimumFractionDigits = 2
        return numberFormatter
    }
    
}
