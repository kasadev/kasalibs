//
//  Configuration.swift
//  KasaLib
//
//  Created by Suleyman Calik on 27.11.2015.
//  Copyright © 2015 Kasa. All rights reserved.
//

import Foundation
import RealmSwift


private var internalRealm:Realm!
private let schemaVersion:UInt64 = 14

public var _realm:Realm! {
    
    if internalRealm == nil {
        refreshRealm()
    }
    
    return internalRealm
}


public func refreshRealm() {
    do {
        let config = Realm.Configuration(schemaVersion:schemaVersion)
        Realm.Configuration.defaultConfiguration = config
        try internalRealm = Realm()
    }
    catch {
        print("Cannot create Realm instance!")
    }
}


public extension Results where T:Object {
    
    public func toArray(limit:Int?=nil) -> [T] {
        
        var objects = [T]()
        
        for (index,object) in self.enumerated() {
            if let limit = limit, index >= limit {
                break
            }
            objects.append(object)
        }
        
        return objects
    }
}

public extension List where T:Object {
    
    public func toArray(limit:Int?=nil) -> [T] {
        
        var objects = [T]()
        
        for (index,object) in self.enumerated() {
            if let limit = limit, index >= limit {
                break
            }
            objects.append(object)
        }
        
        return objects
    }
}


