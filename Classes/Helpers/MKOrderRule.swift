//
//  MKOrderRule.swift
//  Kasa
//
//  Created by Suleyman Calik on 19/03/15.
//  Copyright (c) 2015 Kasa. All rights reserved.
//

import UIKit

public class MKOrderRule: NSObject {
   
    var orderBy:String
    var ascending:Bool

    public init(orderBy:String, ascending:Bool) {
        self.orderBy = orderBy
        self.ascending = ascending
    }
    
}
