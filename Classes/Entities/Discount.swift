//
//  Discount.swift
//  KasaLib
//
//  Created by Suleyman Calik on 20.11.2015.
//  Copyright © 2015 Kasa. All rights reserved.
//

import ObjectMapper

@objc public enum DiscountType:Int {
    case percentage
    case amount
}

public class Discount: QuickKeyItem {

    public dynamic var amount:Double = 0
    public dynamic var type:DiscountType = .percentage

    public required convenience init?(map: Map){
        self.init()
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        amount  <-  map["amount"]
        type    <-  map["type"]
    }

    override public func itemType() -> String {
        return "Discount"
    }


}
