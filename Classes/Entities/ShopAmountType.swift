//
//  ShopAmountType.swift
//  Kasa
//
//  Created by Suleyman Calik on 12.12.2015.
//  Copyright © 2015 Kasa. All rights reserved.
//

import ObjectMapper
import RealmSwift

public class ShopAmountType: MKObject {

    public dynamic var index:Int = 0
    public dynamic var name:String = ""
    public dynamic var isCrossChecked = false

    public dynamic var shopId:String = ""
    public dynamic var amountTypeId:String = ""
    public dynamic var amountType:AmountType!

    public var shop:Shop!          { return Shop.withId(shopId)         }

    
    public required convenience init?(map: Map){
        self.init()
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        index           <-  map["index"]
        name            <-  map["name"]
        shopId          <-  map["shopId"]
        isCrossChecked  <-  map["isCrossChecked"]
        amountTypeId    <-  map["amountTypeId"]
    }

    public override class func ignoredProperties() -> [String] {
        return ["shop"]
    }

    public override class func url() -> String {
        return "ShopAmountType"
    }
    
    public class func crossCheckPaymentTypes() -> [AmountType] {
        let list = objects(ShopAmountType.self, filter:"isCrossChecked = 1 && amountType.isPaymentType = 1", sorting:(sortBy:"index", ascending:true))
        return returnAmountTypesOfList(list)
    }
    
    public class func paymentTypes() -> [AmountType] {
        let list = objects(ShopAmountType.self, filter:"amountType.isPaymentType = 1", sorting:(sortBy:"index", ascending:true))
        return returnAmountTypesOfList(list)
    }
    
    
    fileprivate class func returnAmountTypesOfList(_ list:[ShopAmountType]) -> [AmountType] {
        var types = [AmountType]()
        for sat in list {
            types.append(sat.amountTypeCustom())
        }
        return types
    }
    
    
    public class func amountTypeWithId(_ id:String) -> AmountType! {
        
        guard let sat = objects(ShopAmountType.self, filter:"amountTypeId = '\(id)'").first else {
            return AmountType.withId(id)
        }
        
        return sat.amountTypeCustom()
    }
    
    
    fileprivate func amountTypeCustom() -> AmountType! {
        
        let at = AmountType()
        at.name = name
        at.isCrossChecked = isCrossChecked
        at.index = index
        
        at.isDefault = amountType.isDefault
        at.isPaymentType = amountType.isPaymentType
        at.id = amountType.id
        at.createdAt = amountType.createdAt
        at.updatedAt = amountType.updatedAt
        at.active = amountType.active
        
        return at
        
    }
    
}
