//
//  UserSettings.swift
//  KasaLib
//
//  Created by Alisan Dagdelen on 1/17/17.
//  Copyright © 2017 Kasa. All rights reserved.
//

import Foundation
import ObjectMapper

public class UserSettings:MKObject {
    
    public dynamic var photoUrl:String = ""
    public dynamic var city:String = ""
    public dynamic var bkmEnabled:Bool = false
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        photoUrl   <-  map["photoUrl"]
        city       <-  map["city"]
        bkmEnabled <-  map["bkmEnabled"]
        
    }
    public required convenience init?(map: Map){
        self.init()
    }
    
    
}
