//
//  AccessToken.swift
//  KasaLib
//
//  Created by Suleyman Calik on 13.11.2015.
//  Copyright © 2015 Kasa. All rights reserved.
//

import UIKit
import RealmSwift
import ObjectMapper

public class AccessToken: MKObject {

    public dynamic var created:String = ""
    public dynamic var ttl:Int = 0
    public dynamic var userId:String = ""

    public required convenience init?(map: Map){
        self.init()
    }
    
    public override func mapping(map: Map) {
        
        id      <-  map["id"]
        created <-  map["created"]
        ttl     <-  map["ttl"]
        userId  <-  map["userId"]
        
    }

    public class func currentToken() -> AccessToken! {
        return objects(AccessToken.self).first
    }

}
