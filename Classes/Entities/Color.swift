//
//  Color.swift
//  Kasa
//
//  Created by Suleyman Calik on 22/05/15.
//  Copyright (c) 2015 Kasa. All rights reserved.
//

import ObjectMapper
import UIKit

public class Color: MKObject {
   
    public dynamic var red:Int = 0
    public dynamic var green:Int = 0
    public dynamic var blue:Int = 0
    public dynamic var index:Int = 0
    
    
    public required convenience init?(map: Map){
        self.init()
    }
    
//    public required init?(map: Map) {
//        super.init(map: map)
//    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        red     <-  map["red"]
        green   <-  map["green"]
        blue    <-  map["blue"]
        index   <-  map["index"]
    }
    
    
    public func uicolor() -> UIColor {
        return UIColor(red:CGFloat(red)/255.0, green:CGFloat(green)/255.0, blue:CGFloat(blue)/255.0, alpha:1.0)
    }


}
