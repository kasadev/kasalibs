//
//  SalesGoal.swift
//  KasaLib
//
//  Created by Suleyman Calik on 26.01.2016.
//  Copyright © 2016 Kasa. All rights reserved.
//

import ObjectMapper

public class SalesGoal: QuickKeyItem {

    public dynamic var productId: String = ""
    public dynamic var goal:Int = 0

    public required convenience init?(map: Map){
        self.init()
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        productId   <-  map["productId"]
        goal        <-  map["goal"]
    }
}
