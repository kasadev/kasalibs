//
//  AmountType.swift
//  Kasa
//
//  Created by Suleyman Calik on 27/07/15.
//  Copyright (c) 2015 Kasa. All rights reserved.
//

import ObjectMapper
import RealmSwift

public class AmountType:MKObject {
    
    enum AmountId:String {
        
        case Cash           = "1"
        case CreditCard     = "2"
        case Ticket         = "3"
        case Sodexo         = "4"
        case Multinet       = "6"
        case Setcard        = "7"
        case Winwin         = "8"
        case EFT            = "9"
        
        case OpeningAmount  = "10"
        case InDrawer       = "11"
        case Expense        = "12"
        case ReceiptAmount  = "13"
        case PrepaidTicket  = "14"
        
        func description() -> String {
            return self.rawValue
        }
    }
    
    public dynamic var index:Int = 0
    public dynamic var name:String = ""
    public dynamic var isCrossChecked = false
    public dynamic var isDefault = false
    public dynamic var isPaymentType = false
    
    
    public required convenience init?(map: Map){
        self.init()
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
    }
    
    public class func Cash() -> AmountType! {
        return withId(AmountId.Cash.rawValue)
    }
    
    public class func Expense() -> AmountType! {
        return withId(AmountId.Expense.rawValue)
    }
    
    public class func InDrawer() -> AmountType! {
        return withId(AmountId.InDrawer.rawValue)
    }
    
    public class func ReceiptAmount() -> AmountType! {
        return withId(AmountId.ReceiptAmount.rawValue)
    }
    
    public class func OpeningAmount() -> AmountType! {
        return withId(AmountId.OpeningAmount.rawValue)
    }
    
    public class func shiftClosingTypes() -> [AmountType] {
        var types = [AmountType]()
        if let inDrawer = InDrawer() {
            types.append(inDrawer)
        }
        if let receipt = ReceiptAmount() {
            types.append(receipt)
        }
        return types
    }
    
    public class func allPaymentTypes() -> [AmountType] {
        return objects(AmountType.self, filter:"isPaymentType = 1", sorting:(sortBy:"index", ascending:true))
    }
    
    public class func allObjects() -> [AmountType] {
        return objects(AmountType.self, sorting:(sortBy:"index", ascending:true))
    }
    
    public class func withIndex(_ index:Int) -> AmountType! {
       //return objects(AmountType.self, sorting: nil)
        //return objects(AmountType.self, filter: "index = \(index)", sorting: )
        return objects(AmountType.self, filter:"index = \(index)", sorting:nil).first
//        return objects("index = \(index)", sorting: AmountType.self).first
    }
    
    
}

