//
//  Product.swift
//  Kasa
//
//  Created by Suleyman Calik on 15/05/15.
//  Copyright (c) 2015 Kasa. All rights reserved.
//

import ObjectMapper
import RealmSwift

public class Product:QuickKeyItem {
   
    public dynamic var price:Double = 0
    public dynamic var categoryId:String = ""
    public dynamic var supplierId:String = ""
    public dynamic var abbreviation:String = ""
    
    public dynamic var topIndex:Int = -1

    public var category:ProductCategory! { return ProductCategory.withId(categoryId)}

    
    public required convenience init?(map: Map){
        self.init()
    }

    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        price           <-  map["price"]
        categoryId      <-  map["categoryId"]
        supplierId      <-  map["supplierId"]
        abbreviation    <-  map["abbreviation"]
        
    }

    override public func itemType() -> String {
        return "Product"
    }

    override public static func ignoredProperties() -> [String] {
        return ["color", "shop", "category"]
    }

    public class func topProducts(ofShop shopId:String, limit:Int) -> [Product] {
        return objects(Product.self, filter:"shopId = '\(shopId)' AND topIndex != -1", sorting:(sortBy:"topIndex", ascending:true), limit:limit)
    }

}
