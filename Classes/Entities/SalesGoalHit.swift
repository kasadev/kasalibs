//
//  SalesGoalHit.swift
//  KasaLib
//
//  Created by Suleyman Calik on 26.01.2016.
//  Copyright © 2016 Kasa. All rights reserved.
//

import ObjectMapper

public class SalesGoalHit: MKObject {

    public dynamic var goalId: String = ""
    public dynamic var shiftId: String = ""
    public dynamic var hitCount:Int = 0
    public dynamic var index:Int = 0

    public required convenience init?(map: Map){
        self.init()
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        goalId      <-  map["goalId"]
        shiftId     <-  map["shiftId"]
        hitCount    <-  map["hitCount"]
        index       <-  map["index"]
    }

    
}
