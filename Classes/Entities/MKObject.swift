//
//  MKObject.swift
//  Kasa
//
//  Created by Suleyman Calik on 15/05/15.
//  Copyright (c) 2015 Kasa. All rights reserved.
//

import ObjectMapper
import RealmSwift



public func == (left:MKObject, right:MKObject) -> Bool {
    return left.id == right.id
}

func objectWithId<T:Object>(_ type:T.Type, id:String) -> T? {
    return _realm.object(ofType: type, forPrimaryKey:id as AnyObject)
}



class MKDateTransform:TransformType {
    
    typealias Object = Date
    typealias JSON = String
    
    

    func transformFromJSON(_ value: Any?) -> Object? {
        if let timeStr = value as? String {
            return Date.dateFromServerString(timeStr) as MKDateTransform.Object?
        }
        return nil
    }
    
    func transformToJSON(_ value: Object?) -> JSON? {
        if let date = value {
            return date.serverString(NSTimeZone.system)
        }
        
        return ""
    }

}

public class MKObject:Object, Mappable {
    
    public dynamic var id:String = ""
    public dynamic var createdAt:Date!
    public dynamic var updatedAt:Date!
    public dynamic var active:Bool = false

    public override static func primaryKey() -> String? {
        return "id"
    }


    required convenience public init?(map: Map) {
        self.init()
    }

    
    public func mapping(map: Map) {
        
        if id == "" || map.mappingType == .toJSON {
            id  <- map["id"]
        }
        createdAt   <-  (map["createdAt"], MKDateTransform())
        updatedAt   <-  (map["updatedAt"], MKDateTransform())
        active      <-  map["active"]
    }
    
    public class func url() -> String {
        return String(describing: self)
    }
    
    public class func withId(_ id:String) -> Self? {
        return objectWithId(self, id:id)
    }

    public class func count() -> Int {
        return objects(self).count
    }
    
    public class func objects(_ filter:String? = nil, sorting:(sortBy:String, ascending:Bool)? = nil) -> [MKObject] {
        return objects(self, filter:filter, sorting:sorting)
    }

    
    open class func objects<T:MKObject>(_ type:T.Type, filter:String? = nil, sorting:(sortBy:String, ascending:Bool)? = nil, limit:Int?=nil) -> [T] {
        var results = _realm.objects(type)
        if let filter = filter {
            results = results.filter(filter)
        }
        if let sort = sorting {
            results = results.sorted(byProperty: sort.sortBy, ascending:sort.ascending)
        }
        return results.toArray(limit:limit)
    }
    
    public class func activeObjects<T:MKObject>(_ type:T.Type, filter:String? = nil, sorting:(sortBy:String, ascending:Bool)? = nil) -> [T] {
        let newFilter = (filter == nil || (filter?.characters.count)! == 0) ? "active = true" : filter! + "AND active = true"
        return objects(type, filter:newFilter, sorting:sorting)
    }
    
}


