//
//  OrderItem.swift
//  KasaLib
//
//  Created by Suleyman Calik on 20.11.2015.
//  Copyright © 2015 Kasa. All rights reserved.
//

import ObjectMapper

public class OrderItem: MKObject {

    public dynamic var orderId: String = ""
    public dynamic var productId: String = ""
    public dynamic var inventoryId: String = ""
    public dynamic var quickKeyIndex:Int = 0
    public dynamic var count:Int = 0
    public dynamic var rawAmount:Double = 0
    public dynamic var amount:Double = 0
    public dynamic var discount:Double = 0
    public dynamic var discountType:DiscountType = DiscountType.percentage


    public var product: Product!   { return Product.withId(productId)  }
    
    public required convenience init?(map: Map){
        self.init()
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        orderId         <-  map["orderId"]
        productId       <-  map["productId"]
        inventoryId     <-  map["inventoryId"]
        quickKeyIndex   <-  map["quickKeyIndex"]
        count           <-  map["count"]
        rawAmount       <-  map["rawAmount"]
        amount          <-  map["amount"]
        discount        <-  map["discount"]
        discountType    <-  map["discountType"]
        
    }

    public func calculateAmount() {
        if let product = product {
            amount = product.price * Double(count)
        }
        else {
            amount = 0
        }
    }
    
}
