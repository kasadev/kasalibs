//
//  GoOrder.swift
//  KasaLib
//
//  Created by Alisan Dagdelen on 1/18/17.
//  Copyright © 2017 Kasa. All rights reserved.
//

import ObjectMapper
import RealmSwift

public class GoOrder:MKObject {
    
    public dynamic var shopId: String = ""
    public dynamic var note:String = ""
    public dynamic var customerId:String = ""
    
    public dynamic var transactionType = 0
    public dynamic var transactionId = ""

    public dynamic var totalAmount:Double = 0
    public var orderItems = List<OrderItem>()
 
    
    var items:[OrderItem]! {
        didSet {
            orderItems.removeAll()
            if items != nil {
                orderItems.append(objectsIn: items)
            }
        }
    }
    
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        shopId      <-  map["shopId"]
        note        <-  map["note"]
        totalAmount <-  map["totalAmount"]
        customerId  <-  map["customerId"]
        transactionType <- map["transactionType"]
        transactionId   <- map["transactionId"]
        
        if map.mappingType == .fromJSON {
            items   <-  map["orderItems"]
        }
    }
    
    public required convenience init?(map: Map){
        self.init()
    }
    
    override public static func ignoredProperties() -> [String] {
        return ["items"]
    }
    
    public func toDictionary() -> [String : Any] {
        var json = toJSON()
        
        var jsonItems = [[String : Any]]()
        for item in orderItems {
            jsonItems.append(item.toJSON())
        }
        json["orderItems"] = jsonItems
        
        return json
    }

    
    public class func newOrder(shopId:String) -> GoOrder {
        let order = GoOrder()
        order.id = NSUUID().uuidString
        order.createdAt = Date()
        order.updatedAt = Date()
        order.active = true
        order.shopId = shopId
        if let user = User.currentUser() {
            order.customerId = user.id
        }
        return order
    }

    public func addProduct(_ product:Product, count:Int=1) {
        var productAdded = false
        for (index,item) in orderItems.enumerated() {
            if item.productId == product.id {
                if index <= orderItems.count - 1 {
                    orderItems.remove(at:index)
                    item.count += count
                    item.calculateAmount()
                    orderItems.append(item)
                    productAdded = true
                }
                break
            }
        }
        
        if !productAdded {
            let orderItem = OrderItem()
            orderItem.count = count
            orderItem.productId = product.id
            orderItem.calculateAmount()
            orderItem.id = NSUUID().uuidString
            orderItems.append(orderItem)
        }
        
        calculateAmount()
    }
    
    public func calculateAmount() {
        var amount = 0.0
        for item in orderItems {
            amount += item.amount
        }
        
        totalAmount = amount
    }
}
