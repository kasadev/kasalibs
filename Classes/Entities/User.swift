//
//  User.swift
//  Kasa
//
//  Created by Suleyman Calik on 15/05/15.
//  Copyright (c) 2015 Kasa. All rights reserved.
//

import ObjectMapper
import RealmSwift


public enum UserType:Int {
    
    case regular            = 0
    
    case shopEmployee       = 1
    case shopManager        = 2
    case shopOwner          = 3
    
    case companyEmployee    = 6
    case companyManager     = 7
    case companyOwner       = 8
    
    case administrator      = 10
}


public class User: MKObject {

    public dynamic var email:String = ""
    public dynamic var username:String = ""
    public dynamic var firstName:String = ""
    public dynamic var lastName:String = ""
    public dynamic var fullName:String = ""
    public dynamic var gender:String = ""
    public dynamic var fbId:String = ""
    public dynamic var link:String = ""
    public dynamic var locale:String = ""
    public dynamic var settings:UserSettings!
    public dynamic var timezone:Int = 0
    public dynamic var verified:Bool = false
    
    public dynamic var balance:Double = 0

//    public dynamic var permissions:String = ""
    public dynamic var updated_time:String = ""

    public dynamic var fbToken:String = ""
    public dynamic var tokenRefreshDate:String = ""
    public dynamic var tokenExpirationDate:String = ""
    
    public dynamic var type:Int = UserType.regular.rawValue
    
    public dynamic var token:AccessToken?

    public var companies = List<Company>()
    
    var _companies:[Company]! {
        didSet {
            companies.removeAll()
            if _companies != nil {
                companies.append(objectsIn: _companies)
            }
        }
    }
    
    public override class func url() -> String {
        return "Account"
    }
    
    public override class func ignoredProperties() -> [String] {
        return ["_companies"]
    }
    
    public required convenience init?(map: Map){
        self.init()
    }

    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        type        <-  map["type"]
        email       <-  map["email"]
        username    <-  map["username"]
        firstName   <-  map["firstName"]
        lastName    <-  map["lastName"]
        fullName    <-  map["fullName"]
        gender      <-  map["gender"]
        fbId        <-  map["fbId"]
        link        <-  map["link"]
        locale      <-  map["locale"]
        timezone    <-  map["timezone"]
        verified    <-  map["verified"]
        balance     <-  map["balance"]
        settings    <-  map["settings"]
        
        if map.mappingType == MappingType.fromJSON {
            token       <-  map["token"]
            _companies  <-  map["companies"]
        }

        updated_time        <-  map["updated_time"]
        fbToken             <-  map["fbToken"]
        tokenRefreshDate    <-  map["tokenRefreshDate"]
        tokenExpirationDate <-  map["tokenExpirationDate"]
    }
    
    
    public class func currentUser() -> User! {
        return objects(User.self).first
    }

}




