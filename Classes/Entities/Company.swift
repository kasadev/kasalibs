//
//  Company.swift
//  Kasa
//
//  Created by Suleyman Calik on 03/07/15.
//  Copyright (c) 2015 Calik. All rights reserved.
//

import ObjectMapper
import RealmSwift

public class Company: MKObject {
   
    public dynamic var logo:String = ""
    public dynamic var userId:String = ""
    public dynamic var name:String = ""
    public dynamic var isCurrent:Bool = false
    public dynamic var endorsement:Double = 0

    public var shops = List<Shop>()
    
    var _shops:[Shop]! {
        didSet {
            setShops(_shops)
        }
    }
    
    override public static func ignoredProperties() -> [String] {
        return ["_shops"]
    }
    
    public required convenience init?(map: Map){
        self.init()
    }
    
    public func setShops(_ newShops:[Shop]!) {
        shops.removeAll()
        if newShops != nil {
            shops.append(objectsIn: newShops)
        }
    }

//    public var shops:[Shop] {
//        var myShops = [Shop]()
//        for shop in _realm.objects(Shop).filter(String(format:"companyId = '%@'",id)) {
//            myShops.append(shop)
//        }
//        return myShops
//    }
//    
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        logo        <-  map["logo"]
        userId      <-  map["userId"]
        name        <-  map["name"]
        endorsement <-  map["endorsement"]
        _shops      <-  map["shops"]
    }

    
    public class func currentCompany() -> Company! {
        return objects(Company.self).first
    }
    
    public class func allCompanies() -> [Company] {
        return objects(Company.self, sorting:(sortBy:"endorsement", ascending:false))
    }

}
