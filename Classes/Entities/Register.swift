//
//  Register.swift
//  KasaLib
//
//  Created by Suleyman Calik on 24.11.2015.
//  Copyright © 2015 Kasa. All rights reserved.
//

import UIKit
import ObjectMapper

public class Register: MKObject {

    public dynamic var shopId:String = ""
    public dynamic var pushToken:String = ""
    public dynamic var deviceId:String = ""
    public dynamic var isOpen:Bool = false

    public required convenience init?(map: Map){
        self.init()
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        shopId      <-  map["shopId"]
        pushToken   <-  map["pushToken"]
        deviceId    <-  map["deviceId"]
        isOpen      <-  map["isOpen"]
    }
    
}
