//
//  OrderCustomer.swift
//  KasaLib
//
//  Created by Suleyman Calik on 20.11.2015.
//  Copyright © 2015 Kasa. All rights reserved.
//

import ObjectMapper

public enum CustomerType: Int, CustomStringConvertible {
    case man
    case woman
    case kid
    
    public var description : String {
        get {
            switch(self) {
            case .man: return "Man"
            case .woman: return "Woman"
            default: return "Kid"
            }
        }
    }
}


public class OrderCustomer: MKObject {

    public dynamic var orderId: String = ""
    public dynamic var customerType:Int = 0
    public dynamic var count:Int = 0

    public required convenience init?(map: Map){
        self.init()
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        orderId         <-  map["orderId"]
        customerType    <-  map["customerType"]
        count           <-  map["count"]
    }

}
