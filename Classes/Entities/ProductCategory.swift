//
//  ProductCategory.swift
//  Kasa
//
//  Created by Suleyman Calik on 22/05/15.
//  Copyright (c) 2015 Kasa. All rights reserved.
//

import ObjectMapper

public class ProductCategory: ItemCategory {
   
    public required convenience init?(map: Map){
        self.init()
    }

    public override func mapping(map: Map) {
        super.mapping(map: map)
        
    }
    
    override public func itemType() -> String {
        return "ProductCategory"
    }

    
}
