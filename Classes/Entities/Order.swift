//
//  Order.swift
//  Kasa
//
//  Created by Suleyman Calik on 20/06/15.
//  Copyright (c) 2015 Calik. All rights reserved.
//

import ObjectMapper
import RealmSwift

public class Order:MKObject {
   
    public dynamic var shiftId:String = ""
    public dynamic var shopId: String = ""
    public dynamic var cashierId: String = ""
    public dynamic var registerId:String = ""
    public dynamic var note:String = ""
    public dynamic var amountTypeId:String = ""
    
    public dynamic var rawAmount:Double = 0
    public dynamic var totalAmount:Double = 0

    public dynamic var enteredDiscount:Double = 0
    public dynamic var actualDiscount:Double = 0

    public dynamic var cancelled = false
    
    
    public var orderCustomers = List<OrderCustomer>()
    public var orderItems = List<OrderItem>()
    
    var items:[OrderItem]! {
        didSet {
            orderItems.removeAll()
            orderItems.append(objectsIn: items)
        }
    }
    
    var customers:[OrderCustomer]! {
        didSet {
            orderCustomers.removeAll()
            orderCustomers.append(objectsIn: customers)
        }
    }

    override public static func ignoredProperties() -> [String] {
        return ["items", "customers"]
    }

    
    public required convenience init?(map: Map){
        self.init()
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        shiftId     <-  map["shiftId"]
        shopId      <-  map["shopId"]
        cashierId   <-  map["cashierId"]
        registerId  <-  map["registerId"]
        note        <-  map["note"]
        amountTypeId    <-  map["amountTypeId"]
        
        rawAmount   <-  map["rawAmount"]
        totalAmount <-  map["totalAmount"]

        enteredDiscount <-  map["enteredDiscount"]
        actualDiscount  <-  map["actualDiscount"]
        
        cancelled   <-  map["cancelled"]
        
        items       <-  map["orderItems"]
        customers   <-  map["orderCustomers"]
    }
    
    public class func validOrders() -> [Order] {
        return objects(Order.self).filter{ $0.cancelled == false }
    }

    public class func cancelledOrders() -> [Order] {
        return objects(Order.self).filter{ $0.cancelled == false }
    }

    public class func filterValidOrders(_ orders:[Order]) -> [Order] {
        return orders.filter{ $0.cancelled == false }
    }

    public class func filterCancelledOrders(_ orders:[Order]) -> [Order] {
        return orders.filter{ $0.cancelled == true }
    }
    
    public func addProduct(_ product:Product, count:Int=1) {
        var productAdded = false
        for (index,item) in orderItems.enumerated() {
            if item.productId == product.id {
                if index <= orderItems.count - 1 {
                    orderItems.remove(at:index)
                    item.count += count
                    item.calculateAmount()
                    orderItems.append(item)
                    productAdded = true
                }
                break
            }
        }
        
        if !productAdded {
            let orderItem = OrderItem()
            orderItem.count = count
            orderItem.productId = product.id
            orderItem.calculateAmount()
            orderItems.append(orderItem)
        }
        
        calculateAmount()
    }
    
    public func calculateAmount() {
        var amount = 0.0
        for item in orderItems {
            amount += item.amount
        }
        
        rawAmount = amount
        totalAmount = amount
    }
}




