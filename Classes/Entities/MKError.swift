//
//  MKError.swift
//  KasaLib
//
//  Created by Suleyman Calik on 8.12.2015.
//  Copyright © 2015 Kasa. All rights reserved.
//

import ObjectMapper

public class MKError: MKObject {

    public var name:String!
    public var status:Int!
    public var message:String!
    public var statusCode:Int!
    public var code:String!
    public var stack:String!

    
    public required convenience init?(map: Map){
        self.init()
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        name        <-  map["name"]
        status      <-  map["status"]
        message     <-  map["message"]
        statusCode  <-  map["statusCode"]
        code        <-  map["code"]
        stack       <-  map["stack"]

    }
    
}
