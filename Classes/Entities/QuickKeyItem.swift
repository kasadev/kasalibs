//
//  QuickKeyItem.swift
//  KasaLib
//
//  Created by Suleyman Calik on 24.01.2016.
//  Copyright © 2016 Kasa. All rights reserved.
//

import ObjectMapper

public class QuickKeyItem: MKObject {

    public dynamic var shopId:String = ""
    public dynamic var name:String = ""
    public dynamic var colorId:String = ""

    public var color:Color! { return Color.withId(colorId)  }
    public var shop:Shop!   { return Shop.withId(shopId)    }

    public required convenience init?(map: Map){
        self.init()
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)

        name            <-  map["name"]
        shopId          <-  map["shopId"]
        colorId         <-  map["colorId"]
    }
    
    public func itemType() -> String {
        assertionFailure("Ovverride itemType()")
        return ""
    }

}
