//
//  Device.swift
//  KasaLib
//
//  Created by Suleyman Calik on 30.01.2016.
//  Copyright © 2016 Kasa. All rights reserved.
//

import ObjectMapper

public class Device: MKObject {
    
    public dynamic var userId:String = ""
    public dynamic var deviceId:String = ""
    public dynamic var pushToken:String = ""
    public dynamic var brand:String = ""
    public dynamic var model:String = ""
    public dynamic var osName:String = ""
    public dynamic var osVersion:String = ""
    public dynamic var appVersion:String = ""
    public dynamic var isSimulator = false

    
    public required convenience init?(map: Map){
        self.init()
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        userId      <-  map["userId"]
        deviceId    <-  map["deviceId"]
        pushToken   <-  map["pushToken"]
        brand       <-  map["brand"]
        model       <-  map["model"]
        osName      <-  map["osName"]
        osVersion   <-  map["osVersion"]
        appVersion  <-  map["appVersion"]
        isSimulator <-  map["isSimulator"]
    }

}
