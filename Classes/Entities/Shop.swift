//
//  Shop.swift
//  Kasa
//
//  Created by Suleyman Calik on 15/05/15.
//  Copyright (c) 2015 Kasa. All rights reserved.
//

import ObjectMapper
import RealmSwift

public class Shop: MKObject {
   
    public dynamic var name:String = ""
    public dynamic var logo:String = ""
    public dynamic var companyId:String = ""
    public dynamic var endorsement:Double = 0
    public dynamic var isCurrent:Bool = false
    public dynamic var location:String = ""
    public dynamic var companyName:String = ""
    public dynamic var companyLogo:String = ""
    
    public var registers = List<Register>()
    
    var _registers:[Register]! {
        didSet {
            setRegisters(_registers)
        }
    }
    
    override public static func ignoredProperties() -> [String] {
        return ["_registers"]
    }

    public required convenience init?(map: Map){
        self.init()
    }

    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        name        <-  map["name"]
        logo        <-  map["logo"]
        companyId   <-  map["companyId"]
        endorsement <-  map["endorsement"]
        _registers  <-  map["registers"]
        location    <-  map["location"]
        companyName <-  map["company.name"]
        companyLogo <-  map["company.logo"]
        
    }

    public class func currentShop() -> Shop! {
        return objects(Shop.self).first
    }
    
    
    public func setRegisters(_ newRegisters:[Register]!) {
        registers.removeAll()
        if newRegisters != nil {
            registers.append(objectsIn: newRegisters)
        }
    }
}
