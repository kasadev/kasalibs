//
//  Shift.swift
//  KasaLib
//
//  Created by Suleyman Calik on 26.01.2016.
//  Copyright © 2016 Kasa. All rights reserved.
//

import ObjectMapper

public class Shift: MKObject {

    public dynamic var shopId: String = ""
    public dynamic var registerId:String = ""
    public dynamic var whoOpenedId: String = ""
    public dynamic var whoClosedId: String! = ""
    public dynamic var comment: String! = ""
    public dynamic var openingAmount:Double = 0
    public dynamic var openingDate:NSDate!
    public dynamic var closingDate:NSDate!

    
    public required convenience init?(map: Map){
        self.init()
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        shopId          <-  map["shopId"]
        registerId      <-  map["registerId"]
        whoOpenedId     <-  map["whoOpenedId"]
        whoClosedId     <-  map["whoClosedId"]
        comment         <-  map["comment"]
        openingAmount   <-  map["openingAmount"]
        openingDate     <-  map["openingDate"]
        closingDate     <-  map["closingDate"]
    }
}
